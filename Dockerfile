FROM maven:3-openjdk-11

RUN apt-get update && apt-get install -y python3-pip python3-venv
RUN update-alternatives --install /usr/bin/python python /usr/bin/python3.9 2
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
RUN unzip awscliv2.zip
RUN ./aws/install

ENTRYPOINT ["/usr/local/bin/mvn-entrypoint.sh"]
CMD ["mvn"]